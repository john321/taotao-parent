package com.lanou3g;

// 有界泛型
public class Plate2<T extends Fruit> {

	private T fruit;
	
	public Plate2() {}
	
	public Plate2(T fruit) {
		this.fruit = fruit;
	}
	
	// 泛型方法
//	public <T> void set(T fruit) {
//		System.out.println(fruit);
//	}
	
	// 普通方法
	public void set(T fruit) {
		System.out.println(fruit.getClass());
		this.fruit = fruit;
	}
	
	// 普通方法
	public T get() {
		return this.fruit;
	}
	
}



