package com.lanou3g;

// 泛型类
public class Plate<T> {

	private T fruit;
	
	public Plate() {}
	
	public Plate(T fruit) {
		this.fruit = fruit;
	}
	
	// 泛型方法
//	public <T> void set(T fruit) {
//		System.out.println(fruit);
//	}
	
	// 普通方法
	public void set(T fruit) {
		System.out.println(fruit.getClass());
//		this.fruit = fruit;
	}
	
	// 普通方法
	public T get() {
		return this.fruit;
	}
	
}


// 普通类中也可以定义泛型方法
class Test {
	// 方法泛型的类型取决于调用方法时传入的泛型参数的实际类型
	public <T> T test(String str, T t) {
		return t;
	}
}


