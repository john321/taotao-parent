package com.lanou3g;

import java.lang.reflect.Method;
import java.util.List;

/**
 * 验证泛型的类型擦除（语法糖）
 * @author John
 *
 */
public class TestMain2 {

	public static void main(String[] args) throws Exception {
		
		Plate<Apple> plate = new Plate<Apple>();
		plate.set(new Apple());
		System.out.println("反射操作前，盘子中的水果：" + plate.get());
		
		
		// 利用反射绕过泛型限制，往Plate<Apple>中存一个Banana
		Banana banana = new Banana();
		Method setMethod = Plate.class.getDeclaredMethod("set", Object.class);
		setMethod.invoke(plate, banana);
		System.out.println("反射操作后，盘子中的水果：" + plate.get());
		// 结论： Java的泛型仅仅是在编译期间执行检查，运行期无法保证数据的正确性
		
		
		Apple apple = plate.get();
		System.out.println(apple);
		
		
	}
	
}
